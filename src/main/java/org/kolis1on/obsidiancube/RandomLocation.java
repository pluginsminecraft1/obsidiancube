package org.kolis1on.obsidiancube;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;
import org.kolis1on.obsidiancube.utils.Point2Dim;
import org.kolis1on.obsidiancube.utils.Region2Dim;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public final class RandomLocation {

    private final World world;

    private int depth = -1;

    private Region2Dim region;
    private boolean ignoreRegions;


    public RandomLocation(World world) {
        this.world = world;
    }
    
    public RandomLocation withDepth(int depth) {
        this.depth = depth;
        return this;
    }

    public RandomLocation withRegion(Region2Dim region) {
        this.region = region;
        return this;
    }

    public RandomLocation ignoreRegions() {
        this.ignoreRegions = true;
        return this;
    }

    private CompletableFuture<Location> findLocation(int depth) {
        if (this.depth != -1 && depth >= this.depth) {
            return CompletableFuture.completedFuture(null);
        }

        final int x, z;

        if (region == null) {
            WorldBorder border = world.getWorldBorder();
            ThreadLocalRandom random = ThreadLocalRandom.current();

            int size = (int) border.getSize();

            int radius = size / 2;

            x = random.nextInt(-radius, radius);
            z = random.nextInt(-radius, radius);
        } else {
            Point2Dim randomPoint = region.getRandomPoint();

            x = randomPoint.getX();
            z = randomPoint.getZ();
        }

        return world.getChunkAtAsync(x >> 4, z >> 4).thenCompose(chunk -> {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            int bX = random.nextInt(16);
            int bZ = random.nextInt(16);
            int bY =  random.nextInt(30, 125 - 50);

            Block block = chunk.getBlock(bX, bY, bZ);
            Block down = chunk.getBlock(bX, bY - 1, bZ);

            if (down.getType().isAir() || !down.getType().isSolid()) {
                return findLocation(depth + 1);
            }

            return CompletableFuture.completedFuture(block.getLocation().toCenterLocation());
        });
    }

    public CompletableFuture<Location> findLocation() {
        return findLocation(depth);
    }
}
